/**
 * @name Login
 *
 * @desc Hyper-specific nonsense to submit mileage for travel
 * `node ./dailymileage.js`
 *
 */

// Set some things up.
const calendarDay = process.env.CALENDAR_DAY;
const startLocation = process.env.START_LOCATION;
const endLocation = process.env.END_LOCATION;
//const headless = process.env.LOCAL ? false : true;
const headless = false
const password = process.env.EXPENSIFY_PASS;
const username = process.env.EXPENSIFY_USER;

const expensify_url = 'https://www.expensify.com/expenses';
const puppeteer = require('puppeteer');

// Set up a browser, attempt to login
(async () => {
    const browser = await puppeteer.launch({
        headless: headless,
        args: [
            '--window-size=1920,1080'
        ]
    })
    const pages = await browser.pages()

    try {
        // login things
        await pages[0].setViewport({
            width  : 1920,
            height : 1080
        });
        await pages[0].goto(expensify_url)
        await pages[0].waitForSelector('#js_click_showEmailForm')
        await pages[0].click('#js_click_showEmailForm')
        await pages[0].waitForSelector('#login')
        await pages[0].type('#login', username)
        await pages[0].waitForSelector('#js_click_submitLogin')
        await pages[0].click('#js_click_submitLogin')
        await pages[0].waitForSelector('#js_click_showPasswordForm')
        await pages[0].click('#js_click_showPasswordForm')
        await pages[0].waitForSelector('#password')
        await pages[0].type('#password', password)
        await pages[0].waitForSelector('#js_click_signIn')
        await pages[0].click('#js_click_signIn')

        // open up expenses, submit new expense item
        await pages[0].waitForSelector('#expenseList > button')
        await pages[0].click('#expenseList > button')
        await pages[0].waitForSelector('#expenseList > div > ul > a.list-group-item.is-borderless.mileage')
        await pages[0].click('#expenseList > div > ul > a.list-group-item.is-borderless.mileage')

        // ugly hack because the date field is hard to select
        if (calendarDay) {
            await pages[0].mouse.click(720,440);
            await pages[0].keyboard.press('ArrowRight');
            await pages[0].keyboard.press('ArrowRight');
            await pages[0].keyboard.press('ArrowRight');
            await pages[0].keyboard.press('ArrowRight');
            await pages[0].keyboard.press('ArrowRight');
            await pages[0].keyboard.press('ArrowRight');
            await pages[0].keyboard.press('ArrowRight');
            await pages[0].keyboard.press('ArrowRight');
            await pages[0].keyboard.press('ArrowRight');
            await pages[0].keyboard.press('ArrowRight');
            await pages[0].keyboard.press('ArrowRight');
            await pages[0].keyboard.press('Backspace');
            await pages[0].keyboard.press('Backspace');
            await pages[0].keyboard.press('Backspace');
            await pages[0].keyboard.press('Backspace');
            await pages[0].keyboard.press('Backspace');
            await pages[0].keyboard.press('Backspace');
            await pages[0].keyboard.press('Backspace');
            await pages[0].keyboard.press('Backspace');
            await pages[0].keyboard.press('Backspace');
            await pages[0].keyboard.press('Backspace');
            await pages[0].keyboard.press('Backspace');
            await pages[0].keyboard.type(calendarDay);
        }

        // create map receipt
        await pages[0].waitForSelector('#expenseDialogContainer > div.floatRight.center > div > div > div > button')
        await pages[0].click('#expenseDialogContainer > div.floatRight.center > div > div > div > button')
        await pages[0].waitForSelector('#map3_destinationContainer > li:nth-child(1) > input')
        await pages[0].type('#map3_destinationContainer > li:nth-child(1) > input', startLocation)
        await pages[0].waitForSelector('#map3_destinationContainer > li:nth-child(2) > input')
        await pages[0].type('#map3_destinationContainer > li:nth-child(2) > input', endLocation)
        await pages[0].waitForSelector('#dialog_map3 > div.dialog_wrapper > div > div.floatLeft.width30 > p > a')
        await pages[0].click('#dialog_map3 > div.dialog_wrapper > div > div.floatLeft.width30 > p > a')
        await pages[0].waitForSelector('#map3_destinationContainer > li:nth-child(3) > input')
        await pages[0].type('#map3_destinationContainer > li:nth-child(3) > input', startLocation)
        await pages[0].waitForSelector('#dialog_map3 > div.dialog_wrapper > div > div.floatLeft.width30 > ol.form.floatRight.one-line > li.submit > button')
        await pages[0].click('#dialog_map3 > div.dialog_wrapper > div > div.floatLeft.width30 > ol.form.floatRight.one-line > li.submit > button')
        await pages[0].click('#newExpense3_customUnitsMileageEditor > div > li.separate > div > div > button > span.caret')

        // submit expense
        await pages[0].waitFor(15000)
        await pages[0].click('#dialog_newExpense3 > div.dialog_footer.textRight > button')
        await pages[0].waitFor(1000)
    } catch (e) {
        console.log('Something failed; ', e);
    }

    await browser.close()
})()
