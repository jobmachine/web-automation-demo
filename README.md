Mileage Bot
=========

This is a simple single-file nodeJS project demonstrating an automated way to interact with Expensify. In this case, I need to submit my mileage every day I go on site with the client, which takes ~15-20 minutes to do. Using puppeteer, and an expensify rule (see screenshot of example rule), this process is a single-line command.

The script pulls its configuration from environment variables. I use a utility called `direnv`, which lets me put variables into a local .envrc which does not get committed.

Example .envrc
---
Edit `.envrc.example`, then remove `.example`.

Usage
---
```
# Initialize
npm i

# Invoke
node dailymileage.js

# Invoke for specific day
CALENDAR_DAY=2019-06-05 node ./dailymileage.js

# Invoke for a week
for i in {17..21}; do CALENDAR_DAY=2019-06-$i node ./dailymileage.js; done
```

Generating Archive
---
`git archive --format zip --output ./mileagebot.zip master`
