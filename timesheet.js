/**
 * @name Login
 *
 * @desc Hyper-specific nonsense to submit mileage for travel
 * `node ./dailymileage.js`
 *
 */

// Set some things up.
const calendarDay = process.env.CALENDAR_DAY;
const startLocation = process.env.START_LOCATION;
const endLocation = process.env.END_LOCATION;
//const headless = process.env.LOCAL ? false : true;
const headless = false
const password = process.env.ONELOGIN_PASS;
const username = process.env.ONELOGIN_USER;

const expensify_url = 'https://corpinfo.onelogin.com/login'
const puppeteer = require('puppeteer');

// Set up a browser, attempt to login
(async () => {
    const browser = await puppeteer.launch({
        headless: headless,
        args: [
            '--window-size=1920,1080'
        ]
    })
    const pages = await browser.pages()

    try {
        // login things
        await pages[0].setViewport({
            width  : 1920,
            height : 1080
        });
        await pages[0].goto(onelogin_url)
        await pages[0].waitForSelector('#user_email')
        await pages[0].type('#user_email', username)
        await pages[0].waitForSelector('#user_password')
        await pages[0].type('#user_password', password)
        await pages[0].waitForSelector('#user_submit')
        await pages[0].click('#user_submit')

    } catch (e) {
        console.log('Something failed; ', e);
    }

    //await browser.close()
})()
